import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '../../services/translation/translation.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  constructor (
    private translation: TranslationService
  ) { }

  transform(value: any, args?: any): any {
    return this.translation.translate(value);
  }

}
