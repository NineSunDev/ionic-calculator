import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MainViewComponent } from './components/main-view/main-view.component';
import { DisplayComponent } from './components/display/display.component';
import { KeypadComponent } from './components/keypad/keypad.component';
import { TimesPipe } from './pipes/times/times.pipe';
import { KeypadService } from './services/keypad/keypad.service';
import { HttpClientModule } from '@angular/common/http';
import { TranslationService } from './services/translation/translation.service';
import { TranslatePipe } from './pipes/translate/translate.pipe';
import { SettingsService } from './services/settings/settings.service';

@NgModule({
  declarations: [
    MainViewComponent,
    DisplayComponent,
    KeypadComponent,
    TimesPipe,
    TranslatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MainViewComponent),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MainViewComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    KeypadService,
    TranslationService,
    SettingsService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, deps: [SettingsService], useFactory: getLanguage },
  ],
})
export class AppModule {
}
export function getLanguage(settingsService: SettingsService) {
  return settingsService.getLanguage();
}
