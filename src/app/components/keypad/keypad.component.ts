import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { IKey, IKeypadState, KeypadService } from '../../services/keypad/keypad.service';

@Component({
  selector: 'keypad',
  templateUrl: './keypad.component.html',
})
export class KeypadComponent implements OnInit {

  @Input() mode: string;
  public keypad: IKey[][] = null;
  public opHistory: string[] = [];

  constructor(
    private kps: KeypadService,
    private cdr: ChangeDetectorRef
  ) {
    this.kps.state.subscribe((state: IKeypadState) => {
      if (this.keypad !== state.keypad) {
        this.opHistory = state.operationHistory;
        this.keypad = state.keypad;
        this.mode = state.mode;
        this.cdr.detectChanges();
      }
    })
  }

  ngOnInit() {
  }

  onClick(button: IKey) {
    if (button.type === 'symbol') {
      let state = this.kps.state.value;

      this.kps.mutate({
        operation: state.result ? state.result + button.value : state.operation + button.value,
        result: ''
      });
    }

    if (button.type === 'function') {
      this.kps.execute(button.value);
    }
  }
}
