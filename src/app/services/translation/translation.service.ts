import { Injectable } from '@angular/core';
import { ILangTokens } from '../../i18n/lang-tokens';
import { de } from '../../i18n/de';
import { en } from '../../i18n/en';
import { SettingsService } from '../settings/settings.service';

@Injectable()
export class TranslationService {

    private data : {de: ILangTokens, en: ILangTokens} = {
      de: new de(),
      en: new en()
    };

  constructor(private settings: SettingsService) { }

  public translate(value: string): string {
    if (this.data.en[value] !== undefined) {
      if(this.data.hasOwnProperty(this.settings.getLanguage()))
        return this.data[this.settings.getLanguage()][value];

        return this.data['en'][value];
    }

    return value;
  }

}
