import { Component,  OnInit } from '@angular/core';
import { KeypadService } from '../../services/keypad/keypad.service';

@Component({
  selector: 'display',
  templateUrl: './display.component.html',
})
export class DisplayComponent implements OnInit {

  public displayText: string = '';
  public lastOperation: string = '';

  constructor(
    private kps: KeypadService
  ) {
    this.kps.state.subscribe((state) => {
      this.displayText = state.result !== '' ? state.result : state.operation;
      this.lastOperation = state.result !== '' ? state.operationHistory[state.operationHistory.length - 1] : '';
    });
  }

  ngOnInit() {
  }
}
