import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';

export interface IKey {
  text: string;
  type: 'symbol' | 'function';
  value: string;
}

export interface IKeypadState {
  operation: string;
  mode: 'simple' | 'advanced';
  keypad: IKey[][];
  result: string;
  operationHistory: string[];
  resultHistory: string[];
}

export interface IMutateKeypadState {
  operation?: string;
  mode?: 'simple' | 'advanced';
  keypad?: IKey[][];
  result?: string;
  operationHistory?: string[];
  resultHistory?: string[];
}

const defaultOperationState: IKeypadState = {
  operation: '',
  mode: 'simple',
  keypad: null,
  result: '',
  operationHistory: [],
  resultHistory: [],
};

@Injectable()
export class KeypadService {

  public state: BehaviorSubject<IKeypadState> = new BehaviorSubject<IKeypadState>(defaultOperationState);

  constructor(private http: HttpClient) {
    this.loadKeypad(this.state.value.mode);
  }

  public calculate() {
    let state  = this.state.value;

    try {
      this.mutate({
        operation: '',
        result: state.operation ? eval(state.operation) : state.result,
      });
    } catch (e) {
      this.clear('Syntax Error');
    }
  }

  public mutate(newState: IMutateKeypadState, noHistory: boolean = false) {
    let oldState = this.state.value;

    for (let key in newState) {
      if (oldState.hasOwnProperty(key)) {
        if (!noHistory && newState[key] !== '' && key === 'operation' || key === 'result') {
          oldState[key + 'History'].push(newState[key]);
        }

        oldState[key] = newState[key];
      }
    }

    this.state.next(oldState);
  }

  private loadKeypad(config: 'simple' | 'advanced') {
    this.http.get('./assets/keypad-configs/' + config + '.json')
      .subscribe((result: IKey[][]) => {
        this.mutate({
          keypad: result.map((row: IKey[]) => {
            return row.map((button: IKey) => {
              if (button.text.indexOf('ICON:') === 0) {
                button.text = '<i class="fa ' + button.text.slice(5) + '"></i>';
              }

              return button;
            });
          }),
        });
      });
  }

  public execute(func: string) {
    switch (func) {
      case 'execute':
        this.calculate();
        break;

      case 'invert':
        this.invert();
        break;

      case 'clear':
        this.clear();
        break;

      case 'undo':
        this.undo();
        break;

      case 'reset':
        this.reset();
        break;

      default:
        break;
    }
  }

  public invert() {
    let state = this.state.value;
    let str   = state.result ? state.result : state.operation;

    if (state.operation[0] === '-') {
      str = str.slice(1);
    } else {
      str = '-' + str;
    }

    this.mutate({operation: str, result: ''});
  }

  public undo() {
    const state               = this.state.value;
    let lastOperation: string = '';

    do {
      state.operationHistory.pop();
      lastOperation = state.operationHistory[state.operationHistory.length - 1];
    } while (lastOperation === '');

    this.mutate({operation: lastOperation, result: ''}, true);
  }

  public clear(override:string='') {
    this.mutate({operation: override, result: override});
  }

  public reset() {
    let state = this.state.value;
    state.operation = '';
    state.result = '';

    while (state.operationHistory.length !== 0) {
      state.operationHistory.pop();
    }

    while (state.resultHistory.length !== 0) {
      state.resultHistory.pop();
    }

    this.mutate(state);
  }
}
